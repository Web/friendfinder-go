package friendfinder

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"github.com/GoKillers/libsodium-go/cryptobox"
	"github.com/GoKillers/libsodium-go/cryptogenerichash"
	"github.com/GoKillers/libsodium-go/cryptosign"
	"github.com/GoKillers/libsodium-go/cryptostream"
	"github.com/GoKillers/libsodium-go/randombytes"
	"github.com/GoKillers/libsodium-go/sodium"
	"strings"
)

// Message is a message object containing encrypted data
type Message struct {
	Sequence  int64    `json:"sequence"`
	PubKey    []byte   `json:"pubkey"`
	Signature []byte   `json:"signature"`
	Keys      [][]byte `json:"keys"`
	Location  []byte   `json:"location"`
}

// Location contains location information to be shared
type Location struct {
	Time    int64  `json:"time"`
	Room    string `json:"room,omitempty"`
	Geo     string `json:"geo,omitempty"`
	URL     string `json:"url,omitempty"`
	Comment string `json:"comment,omitempty"`
}

// Initialise Sodium
func init() {
	sodium.Init()
}

// Fingerprint generates a human-readable fingerprint for a public key
func Fingerprint(publicKey []byte) string {
	hash, _ := generichash.CryptoGenericHash(16, publicKey, nil)
	fp := make([]string, 8)
	for i := range fp {
		fp[i] = sodium.Bin2hex(hash[2*i : 2*i+2])
	}
	return strings.Join(fp, ":")
}

// KeyPair returns a new key pair
func KeyPair() ([]byte, []byte) {
	sk, pk, _ := cryptosign.CryptoSignKeyPair()
	return sk, pk
}

// GetMessageSigPart returns the bytes from the message for signatures
func GetMessageSigPart(m *Message) []byte {
	// Convert time and keys to []byte
	s := EncodeSeq(m.Sequence, 8)
	k := bytes.Join(m.Keys[:], nil)

	// Append sequence, keys and location
	s = append(s, k...)
	s = append(s, m.Location...)

	return s
}

// EncodeSeq encodes a sequence number into `len` bytes (big-endian).
func EncodeSeq(seq int64, len int) (b []byte) {
	// Create byte slice
	b = make([]byte, len)

	// Put sequence number at the end
	binary.BigEndian.PutUint64(b[len-8:len], uint64(seq))

	return
}

// VerifyMessage verifies the layout and signature for a message
func VerifyMessage(m *Message) error {
	// Check validity of attributes
	if len(m.PubKey) != cryptosign.CryptoSignPublicKeyBytes() ||
		len(m.Signature) != cryptosign.CryptoSignBytes() ||
		len(m.Keys) == 0 ||
		len(m.Location) == 0 {
		return errors.New("invalid attributes")
	}

	// Check signature
	if cryptosign.CryptoSignVerifyDetached(m.Signature, GetMessageSigPart(m), m.PubKey) != 0 {
		return errors.New("invalid signature")
	}

	return nil
}

// EncryptLocation encrypts a location object for a number of friends and returns a Message
func EncryptLocation(l Location, seq int64, publicKey, secretKey []byte, friendKeys [][]byte) *Message {
	// Message
	m := &Message{
		Sequence: seq,
		PubKey:   publicKey,
		Keys:     make([][]byte, len(friendKeys)),
	}

	// Convert location to JSON
	locationJSON, _ := json.Marshal(l)

	// Get key/nonce for encryption of location
	locKey := randombytes.RandomBytes(cryptostream.CryptoStreamKeyBytes())
	locNonce := EncodeSeq(seq, cryptostream.CryptoStreamNonceBytes())

	// Encrypt location symmetrically
	m.Location, _ = cryptostream.CryptoStreamXOR(locationJSON, locNonce, locKey)

	// Generate nonce for asymmetric encryption
	nonce := EncodeSeq(seq, cryptobox.CryptoBoxNonceBytes())

	// Convert key
	secretBoxKey, _ := cryptosign.CryptoSignEd25519SkToCurve25519(secretKey)

	// Encrypt location nonce and key for each friend
	for i, k := range friendKeys {
		boxKey, _ := cryptosign.CryptoSignEd25519PkToCurve25519(k)
		m.Keys[i], _ = cryptobox.CryptoBoxEasy(locKey, nonce, boxKey, secretBoxKey)
	}

	// Sign the message content
	m.Signature, _ = cryptosign.CryptoSignDetached(GetMessageSigPart(m), secretKey)

	return m
}

// DecryptMessage verifies and decrypts a given message, and returns a Location
func DecryptMessage(m *Message, secretKey []byte) (l Location, err error) {
	// Verify message
	if err = VerifyMessage(m); err != nil {
		return
	}

	// Convert keys
	secretBoxKey, _ := cryptosign.CryptoSignEd25519SkToCurve25519(secretKey)
	senderBoxKey, _ := cryptosign.CryptoSignEd25519PkToCurve25519(m.PubKey)

	// Convert seq to nonce
	secNonce := EncodeSeq(m.Sequence, cryptobox.CryptoBoxNonceBytes())

	// Get the secret key
	var secret []byte
	var c int
	for _, k := range m.Keys {
		secret, c = cryptobox.CryptoBoxOpenEasy(k, secNonce, senderBoxKey, secretBoxKey)
		if c == 0 {
			break
		}
	}

	// Check if a valid key was returned
	if c != 0 || len(secret) != cryptostream.CryptoStreamKeyBytes() {
		err = errors.New("invalid key")
		return
	}

	// Decrypt
	nonce := EncodeSeq(m.Sequence, cryptostream.CryptoStreamNonceBytes())
	key := secret
	data, _ := cryptostream.CryptoStreamXOR(m.Location, nonce, key)

	// Decode
	json.Unmarshal(data, &l)

	return
}
