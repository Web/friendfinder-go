package friendfinder

import (
	"bytes"
	"encoding/hex"
	"github.com/GoKillers/libsodium-go/cryptogenerichash"
	"github.com/google/gofuzz"
	"strings"
	"testing"
)

var testCount = 1000

func LocationEqual(l1, l2 Location) bool {
	if l1.Comment == l2.Comment &&
		l1.Geo == l2.Geo &&
		l1.Room == l2.Room &&
		l1.Time == l2.Time &&
		l1.URL == l2.URL {
		return true
	}
	return false
}

func Setup(friendCount uint8) (sk, pk []byte, f [][]byte, l Location, seq int64) {
	// At least one friend is required
	if friendCount == 0 {
		friendCount++
	}

	// Generate keys
	sk, pk = KeyPair()
	f = make([][]byte, friendCount)
	for i := range f {
		_, f[i] = KeyPair()
	}

	// Add self to friends
	f[0] = pk

	// Generate location / sequence number
	fz := fuzz.New()
	fz.Fuzz(&l)
	fz.Fuzz(&seq)

	return
}

func TestFingerprint(t *testing.T) {
	_, pk := KeyPair()

	fp1, _ := hex.DecodeString(strings.Replace(Fingerprint(pk), ":", "", -1))
	fp2, _ := generichash.CryptoGenericHash(16, pk, nil)

	if !bytes.Equal(fp1, fp2) {
		t.Errorf("Fingerprint incorrect: %x != %x", fp1, fp2)
		t.FailNow()
	}
}

func TestCrypto(t *testing.T) {
	// Init fuzzing
	f := fuzz.New()

	// Run tests
	for i := 0; i < testCount; i++ {
		var l1, l2 Location
		var m1, m2 *Message
		var friendCount uint8

		// Generate test info
		f.Fuzz(&friendCount)
		sk, pk, friends, l1, seq := Setup(friendCount)

		// Test encryption
		m1 = EncryptLocation(l1, seq, pk, sk, friends)
		if err := VerifyMessage(m1); err != nil {
			t.Error("Message could not be verified: ", err)
			t.FailNow()
		}

		// Test decryption
		l2, err := DecryptMessage(m1, sk)
		if err != nil {
			t.Error("Failed to decrypt message: ", err)
			t.FailNow()
		}
		if !LocationEqual(l1, l2) {
			t.Errorf("Locations are not the same: %v != %v", l1, l2)
			t.FailNow()
		}

		// Test decryption incorrect message
		f.Fuzz(m2)
		_, err = DecryptMessage(m2, sk)
		if err == nil {
			t.Error("Successfully decrypted invalid message")
			t.FailNow()
		}

		// Test decryption with a corrupted location
		f.Fuzz(&m1.Location)
		_, err = DecryptMessage(m2, sk)
		if err == nil {
			t.Error("Successfully decrypted corrupted message")
			t.FailNow()
		}

		// Test decryption incorrect key
		var isk [64]byte
		f.Fuzz(&isk)
		_, err = DecryptMessage(m1, isk[:])
		if err == nil {
			t.Error("Successfully decrypted with invalid secret key")
			t.FailNow()
		}

		// Test decryption without being a recipient
		_, friends[0] = KeyPair()
		m1 = EncryptLocation(l1, seq, pk, sk, friends)
		_, err = DecryptMessage(m1, sk)
		if err == nil {
			t.Error("Successfully decrypted without being a recipient")
			t.FailNow()
		}

	}
}

func BenchmarkEncryptLocation(b *testing.B) {
	sk, pk, f, l, seq := Setup(5)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		EncryptLocation(l, seq, pk, sk, f)
	}
}

func BenchmarkVerify(b *testing.B) {
	sk, pk, f, l, seq := Setup(5)

	m := EncryptLocation(l, seq, pk, sk, f)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		VerifyMessage(m)
	}
}

func BenchmarkDecryptMessage(b *testing.B) {
	sk, pk, f, l, seq := Setup(5)

	m := EncryptLocation(l, seq, pk, sk, f)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		DecryptMessage(m, sk)
	}
}
