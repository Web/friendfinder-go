package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"git.slxh.eu/web/friendfinder-go"
	"io/ioutil"
	"net/http"
	"os"
	"os/user"
	"path/filepath"
	"runtime"
	"time"
)

// Response for a HTTP request
type Response struct {
	Message string `json:"message"` // Message explaining the HTTP code.
}

var host string
var friends map[string][]byte
var publicKey []byte
var secretKey []byte
var apiVersion = 1

// MessageEndpoint returns the API Message endpoint for a public key
func MessageEndpoint(publicKey []byte) string {
	pk := base64.URLEncoding.EncodeToString(publicKey)
	return fmt.Sprintf("%s/api/v%v/messages/%s", host, apiVersion, pk)
}

// ReadFile reads a file from disk
func ReadFile(path string) []byte {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return data
}

// WriteFile writes a file to disk
func WriteFile(path string, data []byte, perm os.FileMode) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.MkdirAll(filepath.Dir(path), 0755)
		if err != nil {
			panic(err)
		}
	}

	err := ioutil.WriteFile(path, data, perm)
	if err != nil {
		panic(err)
	}
}

// AddFriend adds a friend to a storage file
func AddFriend(name string, publicKey string, path string) {
	pk, _ := base64.StdEncoding.DecodeString(publicKey)
	friends[name] = pk
	SaveFriends(path, friends)
}

// GetMessage retrieves a message from the API
func GetMessage(publicKey []byte) (*friendfinder.Message, error) {
	rs := new(Response)
	ms := new(friendfinder.Message)

	// Perform the HTTP request
	resp, err := http.Get(MessageEndpoint(publicKey))

	// Error
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Check HTTP error
	if resp.StatusCode != 200 {
		json.NewDecoder(resp.Body).Decode(rs)
		return nil, errors.New(rs.Message)
	}

	// Decode message
	json.NewDecoder(resp.Body).Decode(ms)

	return ms, nil
}

// GetFriend retrieves the published message for a friend using the public key.
// (The name is just for friendly output)
func GetFriend(name string, publicKey []byte) {
	// Show name
	fmt.Printf("Name: %s\n", name)

	// Retrieve the message
	ms, err := GetMessage(publicKey)
	if err != nil {
		fmt.Printf("Error: %s\n\n", err)
		return
	}

	// Decrypt object and show info
	if l, err := friendfinder.DecryptMessage(ms, secretKey); err == nil {
		fmt.Printf("Key:  %s\n", friendfinder.Fingerprint(publicKey))
		fmt.Printf("Time: %s\n", time.Unix(l.Time, 0))
		fmt.Printf("Room: %s\n", l.Room)
		fmt.Printf("Geo:  %s\n", l.Geo)
		fmt.Printf("URL:  %s\n", l.URL)
		fmt.Printf("Comment: %s\n\n", l.Comment)
	} else {
		fmt.Printf("Error: %#v\n\n", err)
	}
}

// GetFriends retrieves information for all friends
func GetFriends() {
	for n, f := range friends {
		GetFriend(n, f)
	}
}

// LoadKeys loads a keypair from disk or generates it if it doesn't exist
func LoadKeys(path string) ([]byte, []byte) {
	var sk, pk []byte
	if _, err := os.Stat(path); os.IsNotExist(err) {
		sk, pk = friendfinder.KeyPair()
		WriteFile(path, sk, 0600)
		WriteFile(path+".pub", pk, 0644)
	} else {
		sk = ReadFile(path)
		pk = ReadFile(path + ".pub")
	}
	return sk, pk
}

// LoadFriends loads friends from disk or generates the file if it doesn't exist
func LoadFriends(path string) map[string][]byte {
	var friends map[string][]byte

	// Check if there is a friend store at path
	if _, err := os.Stat(path); os.IsNotExist(err) {
		// List of friends with only yourself
		friends = make(map[string][]byte)
		friends["Self"] = publicKey

		// Write it to the file
		SaveFriends(path, friends)
	} else {
		// Load from disk
		json.Unmarshal(ReadFile(path), &friends)
	}

	return friends
}

// SaveFriends saves friends to a file
func SaveFriends(path string, friends map[string][]byte) {
	data, _ := json.Marshal(&friends)
	WriteFile(path, data, 0644)
}

// Put submits a message to the API
func Put(l *friendfinder.Location) {
	var sequence int64
	// Get next sequence
	ms, err := GetMessage(publicKey)
	if err != nil {
		if err.Error() == "No message for public key." {
			sequence = 0
		}
	} else {
		sequence = ms.Sequence + 1
	}

	// Make a list of keys
	friendKeys := make([][]byte, 0, len(friends))
	for _, f := range friends {
		friendKeys = append(friendKeys, f)
	}

	// Encrypt the location
	m := friendfinder.EncryptLocation(*l, sequence, publicKey, secretKey, friendKeys)
	mj, _ := json.Marshal(m)

	// Create the HTTP request
	req, _ := http.NewRequest("PUT", MessageEndpoint(publicKey), bytes.NewBuffer(mj))

	// Do it
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)

	// Handle errors
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	// Check response
	if resp.StatusCode != 200 {
		fmt.Printf("Error: %v\n", resp.StatusCode)
	}

	// Decode JSON
	var response Response
	json.NewDecoder(resp.Body).Decode(&response)

	// Show response
	fmt.Printf("Response: %s\n", response.Message)
}

func main() {
	// Vars
	var confDir string

	// CLI options
	var infoFlag bool
	var getFlag bool
	var addFlag bool
	var putFlag bool
	var keyFile string
	var friendsFile string
	var friendName string
	var pubKey string

	// Location parameters
	var room string
	var geo string
	var url string
	var comment string

	// Get user info
	u, _ := user.Current()

	// Set configuration directory
	switch runtime.GOOS {
	case "darwin":
		confDir = u.HomeDir + "/Library/Application Support/friendfinder"
	case "linux":
		confDir = os.Getenv("XDG_CONFIG_HOME")
		if confDir == "" {
			confDir = u.HomeDir + "/.config"
		}
		confDir += "/friendfinder"
	default:
		confDir = u.HomeDir + "/.friendfinder"
	}

	// Command line arguments
	// Generic
	flag.StringVar(&host, "host", "https://friendfinder.slxh.eu", "URL to the host of the API")
	flag.StringVar(&keyFile, "key", confDir+"/ed25519", "Path to the key")
	flag.StringVar(&friendsFile, "friends", confDir+"/friends.json", "Path to a file with your friends public keys")

	// Info
	flag.BoolVar(&infoFlag, "info", false, "Show your info")

	// Get
	flag.BoolVar(&getFlag, "get", false, "Get info for your friends")
	flag.StringVar(&friendName, "name", "", "Name of the friend")

	// Add (also uses name)
	flag.BoolVar(&addFlag, "add", false, "Add a friend by public key")
	flag.StringVar(&pubKey, "pubkey", "", "Base64 encoded public key of the friend")

	// Put
	flag.BoolVar(&putFlag, "put", false, "Upload information for your key")
	flag.StringVar(&room, "room", "", "Room name")
	flag.StringVar(&geo, "geo", "", "Geographic coordinates")
	flag.StringVar(&url, "url", "", "URL of an event")
	flag.StringVar(&comment, "comment", "", "Some additional text")

	// Parse flags
	flag.Parse()

	// Load the keys
	secretKey, publicKey = LoadKeys(keyFile)

	// Load friends
	friends = LoadFriends(friendsFile)

	// Set get flag as fallback
	if (infoFlag || addFlag || putFlag) == false {
		getFlag = true
	}

	if infoFlag {
		fmt.Printf("Fingerprint: %s\n", friendfinder.Fingerprint(publicKey))
		fmt.Printf("Public Key:  %s\n", base64.StdEncoding.EncodeToString(publicKey))
		fmt.Printf("Link:        %s\n", MessageEndpoint(publicKey))
	}

	if addFlag {
		AddFriend(friendName, pubKey, friendsFile)
	}

	if putFlag {
		// Create location from arguments
		l := &friendfinder.Location{
			Time:    time.Now().Unix(),
			Room:    room,
			Geo:     geo,
			URL:     url,
			Comment: comment,
		}

		// Update location
		Put(l)
	}

	if getFlag {
		if friendName == "" {
			GetFriends()
		} else {
			GetFriend(friendName, friends[friendName])
		}
	}
}
