package main

import (
	"errors"
	"git.slxh.eu/web/friendfinder-go"
	"log"
	"sync"
)

// Cache is the memory-based storage for messages
type Cache struct {
	lock     sync.RWMutex
	messages map[string]*friendfinder.Message
}

// Global
var cache Cache

// Init initialises the cache
func (c *Cache) Init() {
	c.messages = make(map[string]*friendfinder.Message)
}

// GetMessage retrieves a message from memory
func (c *Cache) GetMessage(pubKey string) (m *friendfinder.Message, err error) {
	// Lock the cache for reading
	c.lock.RLock()
	defer c.lock.RUnlock()

	// Retrieve message
	m, ok := c.messages[pubKey]
	if !ok {
		err = errors.New("not in cache")
	}

	return
}

// StoreMessage stores a message in the cache
func (c *Cache) StoreMessage(pk string, m *friendfinder.Message) error {
	// Lock cache for writing
	cache.lock.Lock()
	defer cache.lock.Unlock()

	// Update cache
	cache.messages[pk] = m

	// Debug cache size
	if debug {
		log.Printf("Cache size: %v", len(cache.messages))
	}

	return nil
}
