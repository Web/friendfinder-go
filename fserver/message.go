package main

import (
	"encoding/base64"
	"errors"
	"git.slxh.eu/web/friendfinder-go"
	"log"
)

// GetMessage retrieve a message from storage
func GetMessage(pubkey []byte) (m *friendfinder.Message, err error) {
	// Get base64 encoded public key
	pk := base64.URLEncoding.EncodeToString(pubkey)

	// Get from cache
	if m, err = cache.GetMessage(pk); err == nil {
		if debug {
			log.Printf("Message retrieved from cache for %s", pk)
		}
		return
	}

	// Get from cold storage
	if m, err = storage.GetMessage(pk); err == nil {
		// Cache the retrieved message
		cache.StoreMessage(pk, m)

		if debug {
			log.Printf("Message retrieved from storage for %s", pk)
		}
		return
	}

	return nil, errors.New("no message exists")
}

// StoreMessage stores a message
func StoreMessage(m *friendfinder.Message) error {
	// Get base64 encoded public key
	pk := base64.URLEncoding.EncodeToString(m.PubKey)

	// Store in cache
	cache.StoreMessage(pk, m)

	// Store in storage
	return storage.StoreMessage(pk, m)
}
