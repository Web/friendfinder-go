package main

import (
	"encoding/base64"
	"encoding/json"
	"github.com/gorilla/mux"
	"git.slxh.eu/web/friendfinder-go"
	"net/http"
)

// NotFoundEndpoint is the endpoint for requests that result in a 404
func NotFoundEndpoint(w http.ResponseWriter, req *http.Request) {
	ResponseMessage(w, "Resource not found.", http.StatusNotFound)
}

// GetMessageEndpoint is the endpoint for GET requests
func GetMessageEndpoint(w http.ResponseWriter, req *http.Request) {
	// Get parameters
	params := mux.Vars(req)
	pk, _ := base64.URLEncoding.DecodeString(params["pubkey"])

	// Retrieve message and return if found
	if m, err := GetMessage(pk); err == nil {
		ResponseOK(w, m)
		return
	}

	ResponseMessage(w, "No message for public key.", http.StatusNotFound)
}

// GetAllMessagesEndpoint is the endpoint for a GET request at the index
func GetAllMessagesEndpoint(w http.ResponseWriter, req *http.Request) {
	// Return all messages in cache
	ResponseOK(w, cache.messages)
}

// UpdateMessageEndpoint is the endpoint for POST/PUT requests
func UpdateMessageEndpoint(w http.ResponseWriter, req *http.Request) {
	// Get parameters
	params := mux.Vars(req)

	// Save into message
	message := new(friendfinder.Message)
	_ = json.NewDecoder(req.Body).Decode(message)

	// Set public key if given
	if pk, ok := params["pubkey"]; ok {
		message.PubKey, _ = base64.URLEncoding.DecodeString(pk)
	}

	// Verify the message
	if err := friendfinder.VerifyMessage(message); err != nil {
		ResponseMessage(w, "Message failed verification.", http.StatusBadRequest)
		return
	}

	// Check if message is newer
	if om, _:= GetMessage(message.PubKey); om != nil && om.Sequence >= message.Sequence {
		ResponseMessage(w, "Message is older than existing message.", http.StatusBadRequest)
		return
	}

	// Store the message
	if err := StoreMessage(message); err != nil {
		ResponseError(w, err)
		return
	}

	// Give response
	ResponseMessage(w, "Success.", http.StatusOK)
}
