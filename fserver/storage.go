package main

import (
	"encoding/json"
	"fmt"
	"git.slxh.eu/web/friendfinder-go"
	"io/ioutil"
	"os"
)

// DirectoryStorage is the disk-based storage for messages
type DirectoryStorage struct {
	Dir string
}

var storage DirectoryStorage

// Init initialises the storage
func (s *DirectoryStorage) Init() {
	// Create directory if it does not exist
	if _, err := os.Stat(s.Dir); os.IsNotExist(err) {
		if err := os.MkdirAll(s.Dir, 0700); err != nil {
			panic(err)
		}
	}
}

// GetMessage retrieves a message
func (s *DirectoryStorage) GetMessage(pubKey string) (*friendfinder.Message, error) {
	path := fmt.Sprintf("%s/%s", s.Dir, pubKey)

	// Read message
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	// Decode message
	m := new(friendfinder.Message)
	if err := json.Unmarshal(data, m); err != nil {
		return nil, err
	}

	return m, nil
}

// StoreMessage stores a message
func (s *DirectoryStorage) StoreMessage(pubKey string, m *friendfinder.Message) error {
	path := fmt.Sprintf("%s/%s", s.Dir, pubKey)

	// Encode message as JSON
	data, err := json.Marshal(m)
	if err != nil {
		return err
	}

	// Write message to disk
	return ioutil.WriteFile(path, data, 0600)
}
