package main

import (
	"encoding/json"
	"log"
	"net/http"
)

// Response is a response to an HTTP request
type Response struct {
	Message string `json:"message"`
}

// ResponseOK writes a JSON encoded interface with an HTTP OK code as response
func ResponseOK(w http.ResponseWriter, v interface{}) {
	ResponseData(w, v, http.StatusOK)
}

// ResponseMessage writes a JSON message with a given code as response
func ResponseMessage(w http.ResponseWriter, message string, code int) {
	ResponseData(w, &Response{message}, code)
}

// ResponseError writes a generic internal server error and logs the error
func ResponseError(w http.ResponseWriter, err error) {
	ResponseMessage(w, "Something went wrong, try again later.", http.StatusInternalServerError)

	log.Printf("Internal server error: %s", err)
}

// ResponseData writes a JSON encoded interface with a given code as response
func ResponseData(w http.ResponseWriter, v interface{}, code int) {
	// Set JSON header
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// Set response code
	w.WriteHeader(code)

	// Write JSON encoded interface
	json.NewEncoder(w).Encode(v)

	// Data logging
	if debug {
		data, _ := json.Marshal(v)
		log.Printf("Response data: %s", data)
	}
}
