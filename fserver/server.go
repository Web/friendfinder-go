package main

import (
	"flag"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

// MaxFileSize is the maximum upload size
const MaxFileSize = 1048576

var debug bool
var verbose bool

func middleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Limit request
		r.Body = http.MaxBytesReader(w, r.Body, MaxFileSize)

		// Log request
		if verbose || debug {
			log.Printf("Request: %s - %s %s", r.RemoteAddr, r.Method, r.RequestURI)
		}

		// Serve request
		h.ServeHTTP(w, r)
	})
}

func main() {
	var listenAddress string
	var publicListing bool

	// Command line options
	flag.BoolVar(&debug, "d", false, "Enable debug output")
	flag.BoolVar(&verbose, "v", false, "Enable verbose output")
	flag.BoolVar(&publicListing, "p", false, "Enable a public listing of messages")
	flag.StringVar(&listenAddress, "l", ":1333", "Address and port to listen on")
	flag.StringVar(&storage.Dir, "c", "cache", "Cache directory")
	flag.Parse()

	// Initialise storage and cache
	cache.Init()
	storage.Init()

	// Routes
	router := mux.NewRouter()
	router.NotFoundHandler = http.HandlerFunc(NotFoundEndpoint)
	router.HandleFunc("/api/v1/messages", UpdateMessageEndpoint).Methods("PUT", "POST")
	router.HandleFunc("/api/v1/messages/{pubkey}", UpdateMessageEndpoint).Methods("PUT")
	router.HandleFunc("/api/v1/messages/{pubkey}", GetMessageEndpoint).Methods("GET")
	//router.HandleFunc("/api/v1/messages/{pubkey}", DeleteMessageEndpoint).Methods("DELETE")

	// Public index route
	if publicListing {
		router.HandleFunc("/api/v1/messages", GetAllMessagesEndpoint).Methods("GET")
	}

	// Start server
	log.Print("Server listening on " + listenAddress)
	http.ListenAndServe(listenAddress, middleware(router))
}
